#ifndef _CUACCL_H_
#define _CUACCL_H_

#include "profile.h"
#include "../linear.h"
#include "../tron.h"
#include <stdio.h>
#include <time.h>
#include <string>

int cuaccl_init();
int cuaccl_destroy();
int cuaccl_preprocess(feature_node **x, double *y, const int l, const int n,
    const bool random);
int cuaccl_orderback(double *w, const int n);

namespace cuaccl_l2r_lr {
int init(feature_node **x, double *y, const int _l, const int _n);
int destroy();
int lazy_label(int *label);
int create(feature_node **sub_x, const double *C);
int Hv(const double *s, double *Hs);
int Xv(const double *v, double *Xv);
int XTv(const double *v, double *XTv);
double fun(const double *w);
int grad(const double *w, double *g);
}
namespace cuaccl_trcg {
int init(const int _n);
int destroy();
int create(function *_fun_obj);
int trcg(const double delta, const double *g, double *s, double *r,
    const double eps_cg);
}
#endif
