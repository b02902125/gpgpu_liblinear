#include "profile.h"

std::unordered_map<std::string, double> TP::total;
std::unordered_map<std::string, int> TP::count;

TP::TP(const std::string _tag="") : tag(_tag) {
    clock_gettime(CLOCK_MONOTONIC, &start);
}
TP::~TP() {
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (double)(finish.tv_sec - start.tv_sec) * 1000.0;
    elapsed += (double)(finish.tv_nsec - start.tv_nsec) / 1000000.0;
    total[tag] += elapsed;
    count[tag] += 1;
    fprintf(stdout, "Profile: %s %.6fms\n", tag.c_str(), elapsed);
}
void TP::show_total() {
    for(auto &tp : TP::total) {
        printf("Profile Total: %s %.6fms %d times\n",
            tp.first.c_str(), tp.second, TP::count[tp.first.c_str()]);
    }
}
