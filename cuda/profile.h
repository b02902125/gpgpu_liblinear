#ifndef _PROFILE_H_
#define _PROFILE_H_

#include <unordered_map>

class TP {
    private:
        const std::string tag;
        struct timespec start, finish;
        double elapsed;

    public:
        static std::unordered_map<std::string, double> total;
        static std::unordered_map<std::string, int> count;

        static void show_total();

        TP(const std::string _tag);
        ~TP();
};

#endif
